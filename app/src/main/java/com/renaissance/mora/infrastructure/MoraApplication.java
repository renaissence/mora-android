package com.renaissance.mora.infrastructure;

import android.app.Application;

/**
 * Created by 305 on 12/15/2016.
 */

public class MoraApplication extends Application {
    private Auth mAuth;


    @Override
    public void onCreate() {
        super.onCreate();
        mAuth = new Auth(this);
    }

    public Auth getmAuth(){
        return mAuth;
    }

}
