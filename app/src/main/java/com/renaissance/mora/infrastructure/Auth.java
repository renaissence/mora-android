package com.renaissance.mora.infrastructure;

import android.content.Context;

/**
 * Created by 305 on 12/15/2016.
 */

public class Auth {
    private final Context context;
    private User user;

    public Auth(Context context) {
        this.context = context;
        user = new User();

    }

    public User getUser() {
        return user;
    }
}
