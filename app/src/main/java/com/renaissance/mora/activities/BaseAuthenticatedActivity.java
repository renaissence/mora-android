package com.renaissance.mora.activities;

import android.content.Intent;
import android.os.Bundle;

/**
 * Created by 305 on 12/15/2016.
 */

public  abstract class  BaseAuthenticatedActivity extends BaseActivity {
    @Override
    protected final void onCreate(Bundle savedState){
        super.onCreate(savedState);

        if (!application.getmAuth().getUser().isLoggedIn()){
            startActivity(new Intent(this, LoginActivity.class));
            finish();
            return;
        }

        onMoraCreate(savedState);
    }

    protected abstract void onMoraCreate(Bundle savedState);
}

