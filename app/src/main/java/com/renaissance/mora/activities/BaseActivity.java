package com.renaissance.mora.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.renaissance.mora.R;
import com.renaissance.mora.infrastructure.MoraApplication;

public class BaseActivity extends AppCompatActivity {

    protected MoraApplication application;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);

        application = (MoraApplication) getApplication();
    }
}
